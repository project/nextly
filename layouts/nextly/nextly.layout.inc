name = Nextly
description = Default layout for NextDrupal.com.
preview = preview.png
template = nextly-layout

; Regions
regions[top]            = Top
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[hero]           = Hero
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[test]           = Testimonial‎
regions[touch]          = Get in Touch
regions[footer]         = Footer

; Stylesheets
stylesheets[all][] = css/layouts/nextly/nextly-layout.css
